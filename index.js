let arr = [1, 2, 3];

// ES-5
for (let i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}

// ES-6
// { done: false, value: 1}
for (let elem of arr) {
  console.log("elem",elem);
}

// объекты и сущности которые можно перебрать циклом for наз-ся 
// итерируемые. Значит что они имеют итератор.
// Итератор это объект который участвует в переборе массива/объектов


// console.log(arr[Symbol.iterator]); // хранит правила перебора

let range = {
  from: 1,
  to: 5,
}

// error
// for (let num of range) {
//   console.log("num", num); // 1, затем 2, 3, 4, 5
// }


console.log(range[Symbol.iterator]); //запишем правила перебора объекта


// сделаем объект range итерируемым
/**
 * 
 * При вызове метода Symbol.iterator перебираемый объект должен возвращать 
 * другой объект («итератор»), который умеет осуществлять перебор
 * 
 * По стандарту у такого объекта должен быть метод next(), который 
 * при каждом вызове возвращает очередное значение и проверяет, окончен ли перебор.
 */
range[Symbol.iterator] = function() {
  // метод должен вернуть объект с методом next()
  return { 
    current: this.from,
    last: this.to,
    next() {
     
      if (this.current <= this.last) {
        return {
          done: false,
          value: this.current++
        };
      } else {
        return {
          done: true
        };
      }
    }

  }
};
for (let elem of range) {
  console.log("obj",elem);
}

range = {
  from: 1,
  to: 5,

  [Symbol.iterator]() {
    return this;
  },
  next() {
    if (this.current === undefined) {
      // инициализация состояния итерации
      this.current = this.from;
    }

    if (this.current <= this.to) {
      return {
        done: false,
        value: this.current++
      };
    } else {
      // очистка текущей итерации
      delete this.current;
      return {
        done: true
      };
    }
  }
};
for (let num of range) {
  console.log("num", num); // 1, затем 2, 3, 4, 5
}


const weekDays = {
  [Symbol.iterator]() {
    return {
      index: 0,
      days: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
      next() {
        if(this.index < this.days.length) {
         return { value: this.days[this.index++], done: false }
        }
        return { done: true }
      }
    }
  }
}
for (const day of weekDays) {
  console.log("day", day);
}

const person = {
  name: "John",
  age: 13
}
// [["name", "John"]]
for (let [key, value] of Object.entries(person)) {
  console.log(key, value);
}

const newArr = Array.from(weekDays);
console.log(newArr);


// у объекта ключи всегда строки
/**Map – это коллекция ключ/значение, как и Object. 
 * Но основное отличие в том, что Map позволяет использовать ключи любого типа */
const person2 = {
  name: "John",
  1: 13
}
const map = new Map();
map.set("name", "John");
const myFunction = function(){
  console.log("Hello world");
}
map.set(myFunction, "person");
console.log(map.get("name"));
console.log(map.get(myFunction));
console.log(map);

/**Объект Set – это особый вид коллекции: «множество» значений (без ключей),
 *  где каждое значение может появляться только один раз. */
let set = new Set();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

// считаем гостей, некоторые приходят несколько раз
set.add(john);
set.add(pete);
set.add(mary);
set.add(john);
set.add(mary);

// set хранит только 3 уникальных значения
console.log(set.size); // 3
console.log(set); // 3

/*Генератор — это процесс, который может быть остановлен и возобновлен*/
/**
 * async/await это генераторы, мы дожидаемся результата и идем исполнять др код
 */
function* generateFunction() {
  yield "John";
  yield "Paul";
  yield "Jane";

  return "Hello world";
}

// const generateFunction = function*() {}

const generatorObj = {
  *generatorMethod() {}
}

class GeneratorClass {
  *generatorMethod(){}
}

const generator = generateFunction();
// console.log(generator.next());
// console.log(generator.next().value);
// console.log(generator.next());
// console.log(generator.next());
// console.log(generator.next());

for (let value of generator) {
  console.log("value==",value);
}

// бесконечный цикл
const incrementer = function*() {
  let i = 0;
  while(true) {
    yield i++;
  }
}
const counter = incrementer();
for (let i = 0; i < 100; i++) {
  console.log(counter.next().value);
}

/**Передача значений в генераторы */
function* generateFunc() {
  
  // console.log(yield);
  // console.log(yield);
  // console.log(yield);

  const a = yield;
  console.log(a + 100);
  console.log(yield);
  console.log(yield);
  console.log(yield);
}
const generator2 = generateFunc();
generator2.next(); // запуск
generator2.next(100);
generator2.next(500);
generator2.next(300);
generator2.next(400);

// останавливаем генератор намеренно
function* generateFunction2() {
  yield "John";
  yield "Paul";
  yield "Jane";
  yield* generateFunction3();
}
const generator3 = generateFunction2();
// console.log(generator3.next().value);
// console.log(generator3.next().value);
// console.log(generator3.return("output value").value);
// console.log(generator3.throw("output value").value);
// console.log(generator3.next().value);

// делегирование генератора
function* generateFunction3() {
  yield "Doe";
  yield "vasya";
}

for (let value of generator3) {
  console.log(value);

}

/**async/await в генераторах */
// const getUsers = async() => {
//   const response = await fetch("https://jsonplaceholder.typicode.com/users");
//   const data = await response.json();

//   return data;
// }

// getUsers.then(users => {
//   console.log(users);
// });

const getUsers = asyncAlt(function*(){
  const response = yield fetch("https://jsonplaceholder.typicode.com/users");
  // { done: false, value: {ok: true, result: []}}
  const data = yield response.json();
  // { done: false, value:  [users]}}

  return data;//{ done: true, value:  [users]}}
});

function asyncAlt(generatorFunc) {
  return function() {
    const generator = generatorFunc();
    const resolve = (next) => {
      if (next.done) {
        return Promise.resolve(next.value);
      }

      return Promise.resolve(next.value).then(response => {
        return resolve(generator.next(response));
      });
    }
// вызываем для активации
    return resolve(generator.next());
  }
}